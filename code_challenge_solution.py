import hashlib

def generate_hexadecimal(input_string):
    # Get the ASCII values of each character in the string and generate a list of these values
    ascii_values = [ord(char) for char in input_string]

    # Multiply each value in the list by the number of characters in the string
    multiplied_values = [value * len(input_string) for value in ascii_values]

    # Find the sum of all numbers in the resulting list
    sum_values = sum(multiplied_values)

    # Generate a SHA256 hash
    hash_object = hashlib.sha256(str(sum_values).encode())

    # Convert the hash to a hexadecimal string
    hexadecimal_string = hash_object.hexdigest()

    return hexadecimal_string


# Example usage
input_string = "CitricSheep"
result = generate_hexadecimal(input_string)
print("Hexadecimal string:", result)

